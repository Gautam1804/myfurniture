import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:furniture_app/pages/main_page.dart';

import 'package:furniture_app/pages/option_page.dart';
import 'package:furniture_app/pages/register_page.dart';
import 'package:furniture_app/widgets/validation.dart';
import 'package:get/get.dart';

// import 'package:furniture_app/register_page.dart';

class LoginPage extends StatelessWidget {
  LoginPage({Key? key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          GestureDetector(
              onTap: () {
                Get.to(OptionPage());
              },
              child: Image.asset("images/cancel.png"))
        ],
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Form(
            key: _formKey,
            child: GestureDetector(
              onTap: () {
                print("ddd");
                FocusScope.of(context).unfocus();
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 20, bottom: 80),
                    child: Text(
                      "Login",
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Label(name: "Your Email"),
                  Ans(
                    ans: "Email is required",
                    require: "",
                    email: "",
                    field: "email",
                    password: '',
                  ),
                  SizedBox(height: 30),
                  Label(name: "Password"),
                  Ans(
                    ans: "Password cannot be empty",
                    require: "Password length should be atleast 6",
                    email: "",
                    field: "password",
                    password: "password",
                  ),
                  TextButton(
                    onPressed: () {
                      print("Forget password");
                    },
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        "Forgot password?",
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  ),
                  // Padding(padding: EdgeInsets.only(top: size.height * .07)),
                  SizedBox(height: size.height * .07),
                  Center(
                    child: SizedBox(
                      height: size.height * .06,
                      width: size.width * .8,
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            Get.to(MainPage());
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                            primary: Color(0xffB86B28)),
                        child: Text(
                          "Login",
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Color(0xffFFFFFF)),
                        ),
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: size.height * .05)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      GestureDetector(
                        onTap: () {
                          print("Facebook");
                        },
                        child: CircleAvatar(
                            backgroundColor: Color(0xff2E4F8E),
                            child: Image(
                              image: AssetImage("images/facebook.png"),
                            )),
                      ),
                      Padding(padding: EdgeInsets.symmetric(horizontal: 15)),
                      GestureDetector(
                        onTap: () {
                          print("Facebook");
                        },
                        child: CircleAvatar(
                            backgroundColor: Color(0xffE3443A),
                            child: Image(
                              image: AssetImage("images/google.png"),
                            )),
                      ),
                    ],
                  ),
                  SizedBox(height: size.height * .20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RichText(
                          text: TextSpan(
                              text: "Don't have an account ?",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 14),
                              children: [
                            TextSpan(
                              text: "  Register",
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  print("Register");
                                  Get.to(RegisterPage());
                                },
                            )
                          ])),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      // bottomNavigationBar: Padding(
      //   padding: EdgeInsets.only(bottom: 30),
      //   child: Row(
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     children: [
      //       RichText(
      //           text: TextSpan(
      //               text: "Don't have an account ?",
      //               style: TextStyle(color: Colors.black, fontSize: 14),
      //               children: [
      //             TextSpan(
      //               text: "  Register",
      //               recognizer: TapGestureRecognizer()
      //                 ..onTap = () {
      //                   print("Register");
      //                   Get.to(RegisterPage());
      //                 },
      //             )
      //           ])),
      //     ],
      //   ),
      // ),
    );
  }
}
