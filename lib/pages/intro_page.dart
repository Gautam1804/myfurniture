import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:furniture_app/controller/introcontroller.dart';
import 'package:furniture_app/pages/option_page.dart';

import 'package:get/get.dart';

class IntroPage extends StatelessWidget {
  final IntroController _con = Get.put(IntroController());
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CarouselSlider(
              options: CarouselOptions(
                  height: size.height * .60,
                  viewportFraction: 1,
                  autoPlay: true,
                  enlargeCenterPage: true,
                  // aspectRatio: 2.0,
                  onPageChanged: (index, reason) {
                    _con.current.value = index;
                  }),
              items: [
                0,
                1,
                2,
              ].map((i) {
                return Builder(
                  builder: (BuildContext context) {
                    return Container(
                        width: Get.width,
                        // margin: EdgeInsets.symmetric(horizontal: 5.0),
                        padding: EdgeInsets.only(right: Get.width * 0.15),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(50)),
                          child: Image(
                            image: AssetImage(_con.introList[i].image),
                            fit: BoxFit.cover,
                          ),
                        ));
                  },
                );
              }).toList(),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 16),
              child: Row(
                children: [
                  indicator(0),
                  SizedBox(
                    width: 5,
                  ),
                  indicator(1),
                  SizedBox(
                    width: 5,
                  ),
                  indicator(2),
                ],
              ),
            ),
            Obx(
              () => Padding(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                child: Text(
                  _con.introList[_con.current.value].name,
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Obx(
              () => Padding(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Text(
                  _con.introList[_con.current.value].subtitle,
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: GestureDetector(
        onTap: () {
          Get.to(OptionPage());
        },
        child: Text(
          "Skip",
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
    );
  }

  Widget indicator(int index) {
    return Obx(
      () => Container(
        height: 5,
        width: 40,
        decoration: BoxDecoration(
            color: _con.current.value == index
                ? Color(0xffB86B28)
                : Color(0xffF0DFD1),
            borderRadius: BorderRadius.all(Radius.circular(50))),
      ),
    );
  }
}
