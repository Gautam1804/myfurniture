import 'package:flutter/material.dart';
import 'package:furniture_app/controller/ordercontroller.dart';
import 'package:furniture_app/pages/checkout_page.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';
import 'package:get/route_manager.dart';

class CartPage extends StatelessWidget {
  final OrderController orderController = Get.put(OrderController());
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Obx(
        () => Column(
          children: [
            SizedBox(
              height: 30,
            ),
            ...orderController.orderList
                .asMap()
                .map((i, value) => MapEntry(
                    i,
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 16),
                      child: Dismissible(
                        key: Key(i.toString()),
                        direction: DismissDirection.endToStart,
                        background: Container(
                          decoration: BoxDecoration(
                            color: Color(0xffE92222),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 40),
                                child: Icon(
                                  Icons.delete,
                                  color: Colors.white,
                                  size: 40,
                                ),
                              ),
                            ],
                          ),
                        ),
                        onDismissed: (direction) {
                          Order removeorder =
                              orderController.orderList.removeAt(i);
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text("Remove ${removeorder.name}"),
                            ),
                          );
                        },
                        child: Container(
                          height: 120,
                          width: 400,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                new BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 5.0,
                                ),
                              ]),
                          child: Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 20),
                                child: Container(
                                    height: size.height < 600 ? 50 : 80,
                                    width: size.height < 600 ? 50 : 80,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(10)),
                                    ),
                                    child:
                                        Image(image: AssetImage(value.image))),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      value.name,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600),
                                    ),
                                    Text(
                                      "\$${value.price.toString()}",
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          "Color: ${value.color}",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w300),
                                        ),
                                        SizedBox(
                                            width: size.height < 600
                                                ? size.width * .1
                                                : size.width * .25),
                                        Obx(() => Row(
                                              children: [
                                                GestureDetector(
                                                  onTap: () {
                                                    value.quntity!.value >= 1
                                                        ? value.quntity!.value--
                                                        // ignore: unnecessary_statements
                                                        : orderController
                                                            .orderList
                                                            .removeAt(i);

                                                    value.quntity!.value >= 1
                                                        // ignore: unnecessary_statements
                                                        ? Null
                                                        // ignore: unnecessary_statements
                                                        : orderController
                                                            .freeShippingAmount
                                                            .value = 40;
                                                  },
                                                  child: Container(
                                                    padding: EdgeInsets.all(2),
                                                    alignment: Alignment.center,
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      border: Border.all(
                                                        color:
                                                            Color(0xffDFDFDF),
                                                      ),
                                                    ),
                                                    child: Icon(
                                                      Icons.remove,
                                                      size: 18,
                                                    ),
                                                  ),
                                                ),
                                                // Icon(Icons
                                                //     .add_circle_outline)

                                                SizedBox(width: 10),
                                                Text(
                                                  value.quntity!.value
                                                      .toString(),
                                                ),
                                                SizedBox(width: 10),
                                                GestureDetector(
                                                    onTap: () {
                                                      value.quntity!.value < 10
                                                          ? value
                                                              .quntity!.value++
                                                          // ignore: unnecessary_statements
                                                          : null;
                                                    },
                                                    child: Container(
                                                      padding:
                                                          EdgeInsets.all(2),
                                                      alignment:
                                                          Alignment.center,
                                                      decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        border: Border.all(
                                                          color:
                                                              Color(0xffDFDFDF),
                                                        ),
                                                      ),
                                                      child: Icon(
                                                        Icons.add,
                                                        size: 18,
                                                      ),
                                                    )),
                                              ],
                                            ))
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )))
                .values
                .toList(),
            SizedBox(
              height: 15,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Shipping Amount",
                        style: TextStyle(fontSize: 14),
                      ),
                      Text(
                          "\$${orderController.calculateTotal().toStringAsFixed(2)}"),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Free Shipping",
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      Obx(
                        () => Text(orderController.orderList.length == 0
                            ? ("\$${0.0}")
                            : ("\$${orderController.freeShippingAmount.toString()}")),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Total Amount:",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      Obx(
                        () => Text(
                          "\$${orderController.orderList.length == 0 ? 0.0 : (double.parse(orderController.calculateTotal().toStringAsFixed(2)) + orderController.freeShippingAmount.value).toStringAsFixed(2)}",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            Padding(padding: EdgeInsets.only(top: 80)),
            Center(
              child: SizedBox(
                height: size.height * .06,
                width: size.width * .8,
                child: ElevatedButton(
                    onPressed: () {
                      Get.to(CheckoutPage());
                    },
                    style: ElevatedButton.styleFrom(primary: Color(0xffB86B28)),
                    child: Center(
                        child: Text(
                      "Check Out",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ))),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Container(
                height: size.height * .06,
                width: size.width * .8,
                decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(width: 2, color: Color(0xffB86B28)),
                        bottom: BorderSide(width: 2, color: Color(0xffB86B28)),
                        left: BorderSide(width: 2, color: Color(0xffB86B28)),
                        right: BorderSide(width: 2, color: Color(0xffB86B28)))),
                child: ElevatedButton(
                    onPressed: () {
                      print("contune shopping");
                      // Get.back();
                    },
                    style: ElevatedButton.styleFrom(primary: Color(0xffFFFFFF)),
                    child: Center(
                        child: Text(
                      "Continue Shopping",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ))),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
