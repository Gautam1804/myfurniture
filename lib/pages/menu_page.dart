import 'package:flutter/material.dart';
import 'package:furniture_app/controller/menucontroller.dart';
import 'package:furniture_app/pages/decore_page.dart';
import 'package:furniture_app/pages/search_page.dart';
import 'package:get/get.dart';

class MenuPage extends StatelessWidget {
  MenuPage({Key? key}) : super(key: key);

  final MenuController menuController = Get.put(MenuController());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
          child: Container(
            height: 50,
            decoration: BoxDecoration(
                color: Color(0xffF5F5F5),
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: TextFormField(
              decoration: InputDecoration(
                  suffixIcon: Image.asset("images/mic.png"),
                  prefixIcon: GestureDetector(
                    onTap: () {
                      Get.to(SearchPage());
                    },
                    child: Image.asset("images/search.png"),
                  ),
                  hintText: "What are you looking for?",
                  hintStyle: TextStyle(color: Colors.black),
                  border: InputBorder.none),
            ),
          ),
        ),
        // SizedBox(
        //   height: 20,
        //   width: 20,
        // ),
        Expanded(
          child: GridView.count(
            childAspectRatio: 0.8,
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            crossAxisCount: 3,
            crossAxisSpacing: 15,
            mainAxisSpacing: 30,
            children: List.generate(
                menuController.menuList.length,
                (index) => GestureDetector(
                      onTap: () {
                        Get.to(
                            DecorePage(
                                // title: menuController.menuList[index].title,
                                ),
                            arguments: menuController.menuList[index].title);
                      },
                      child: Container(
                        height: size.height * .2,
                        width: 100,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                              blurRadius: 5,
                              spreadRadius: 2,
                            )
                          ],
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Image(
                                  image: AssetImage(
                                      menuController.menuList[index].image)),
                            ),
                            
                            Text(
                              menuController.menuList[index].title,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black),
                            ),
                          ],
                        ),
                      ),
                    )),
          ),
        )
      ],
    );
  }
}
