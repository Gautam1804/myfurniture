import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:furniture_app/pages/login_page.dart';
import 'package:furniture_app/pages/register_page.dart';

import 'package:get/get.dart';

class OptionPage extends StatelessWidget {
  const OptionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Column(
        children: [
          Stack(
            children: [
              Container(
                height: size.height,
                width: size.width,
                child: Image(
                  image: AssetImage("images/login.jpg"),
                  fit: BoxFit.cover,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 15,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: size.height * .15,
                    ),
                    Text(
                      "Make superior \nliving way for you",
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    Padding(padding: EdgeInsets.only(top: 15)),
                    Text(
                      "Choose and Get the foremost furniture for your home !!",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: size.height < 600
                          ? size.height * .3
                          : size.height * .45,
                    ),
                    // Padding(padding: EdgeInsets.only(top: size.height * .45)),
                    Center(
                      child: SizedBox(
                        height: 50,
                        width: size.width * .8,
                        child: ElevatedButton(
                          onPressed: () {
                            Get.to(LoginPage());
                          },
                          style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50)),
                              primary: Color(0xffFFFFFF)),
                          child: Text(
                            "Login",
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff000000)),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Center(
                      child: SizedBox(
                        height: 50,
                        width: size.width * .8,
                        child: ElevatedButton(
                          onPressed: () {
                            Get.to(RegisterPage());
                          },
                          style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50)),
                              primary: Color(0xffFFFFFF)),
                          child: Text(
                            "Sign up",
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Color(0xff000000)),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
