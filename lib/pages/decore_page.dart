import 'package:flutter/material.dart';
import 'package:furniture_app/controller/decorecontroller.dart';
import 'package:furniture_app/controller/menucontroller.dart';
import 'package:furniture_app/pages/lamp_page.dart';

import 'package:get/get.dart';

class DecorePage extends StatelessWidget {
  // final String title;

  // DecorePage({Key? key, required this.title}) : super(key: key);
  final DecoreController decoreController = Get.put(DecoreController());
  final MenuController menuController = Get.put(MenuController());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    print(size.height);
    var title = Get.arguments;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Image.asset("images/back.png")),
        title: Center(
          child: Text(
            title.toString(),
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: size.height < 600 ? 18 : 22,
            ),
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Image.asset("images/search.png"),
          )
        ],
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Expanded(
              child: GridView.count(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                crossAxisSpacing: 30,
                mainAxisSpacing: 30,
                crossAxisCount: 2,
                childAspectRatio: .85,
                children: List.generate(
                    decoreController.decoreList.length,
                    (index) => GestureDetector(
                          onTap: () {
                            Get.to(LampPage(),
                                arguments:
                                    decoreController.decoreList[index].name);
                          },
                          child: Container(
                            height: 100,
                            width: 170,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black12,
                                  blurRadius: 5,
                                  spreadRadius: 2,
                                )
                              ],
                            ),
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image(
                                    image: AssetImage(decoreController
                                        .decoreList[index].image),
                                    height: size.height < 600 ? 100 : 150,
                                  ),
                                ),
                                Spacer(),
                                Text(
                                  decoreController.decoreList[index].name,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                          ),
                        )),
              ),
            )
          ],
        ),
      ),
    );
  }
}
