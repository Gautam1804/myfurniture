import 'package:flutter/material.dart';
import 'package:furniture_app/controller/imagescontroller.dart';
import 'package:get/get.dart';

class ImagePage extends StatelessWidget {
  final ImageController imageController = Get.put(ImageController());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Image.asset("images/cancel.png")),
        actions: [Image.asset("images/share.png")],
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      bottomSheet: Padding(
        padding: EdgeInsets.only(bottom: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            indicator(0),
            SizedBox(
              width: 5,
            ),
            indicator(1),
            SizedBox(
              width: 5,
            ),
            indicator(2),
            SizedBox(
              width: 5,
            ),
            indicator(3),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: size.height -
                      AppBar().preferredSize.height -
                      MediaQuery.of(context).viewPadding.top,
                  width: size.width,
                  // color: Colors.amber,
                  color: Colors.transparent,
                ),
                Positioned(
                  // color: Colors.transparent,
                  right: -100,

                  child: Image(
                    height: 450,
                    image: AssetImage("images/belly.png"),
                  ),
                ),
                Row(
                  children: [
                    Container(
                      // color: Colors.pink,
                      color: Colors.transparent,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            child: Text(
                              "Braided\nbelly lamp",
                              style: TextStyle(
                                  fontSize: 22, fontWeight: FontWeight.bold),
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 15),
                                child: Text(
                                  "Colors: ",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Row(
                                children: [
                                  colorsbar(Color(0xffA0865A), 0),
                                  Padding(padding: EdgeInsets.only(left: 2)),
                                  colorsbar(Color(0xff734646), 1),
                                  Padding(padding: EdgeInsets.only(left: 2)),
                                  colorsbar(Color(0xff25B174), 2),
                                  Padding(padding: EdgeInsets.only(left: 2)),
                                  colorsbar(Color(0xff687A2B), 3)
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget colorsbar(Color color, int index) {
    return Obx(
      () => GestureDetector(
        onTap: () {
          imageController.isselected.value = index;
        },
        child: Container(
            height: 35,
            width: 35,
            child: Center(
                child: CircleAvatar(
                    radius: imageController.isselected.value == index ? 14 : 11,
                    backgroundColor: color)),
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                border: Border.all(
                    color: imageController.isselected.value == index
                        ? Color(0xffC8C8C8)
                        : Colors.white,
                    width: 1))),
      ),
    );
  }

  Widget indicator(int index) {
    return Obx(
      () => Container(
        height: 5,
        width: 40,
        decoration: BoxDecoration(
            color: imageController.current.value == index
                ? Color(0xffB86B28)
                : Color(0xffF0DFD1),
            borderRadius: BorderRadius.all(Radius.circular(50))),
      ),
    );
  }
}
