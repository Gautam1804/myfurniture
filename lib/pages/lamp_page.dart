import 'package:flutter/material.dart';
import 'package:furniture_app/controller/decorecontroller.dart';
import 'package:furniture_app/controller/lampcontroller.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:furniture_app/pages/lamp_detail_page.dart';
import 'package:get/get.dart';

class LampPage extends StatelessWidget {
  final LampController lampController = Get.put(LampController());
  final DecoreController decoreController = Get.put(DecoreController());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    var title = Get.arguments;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Image.asset("images/back.png"),
        ),
        title: Center(
          child: Text(
            title.toString(),
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: size.height < 600 ? 18 : 22,
            ),
          ),
        ),
        actions: [
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Image.asset("images/search.png"))
        ],
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 20),
            Expanded(
              child: new StaggeredGridView.countBuilder(
                crossAxisCount: 4,
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                itemCount: lampController.lampList.length,
                staggeredTileBuilder: (int index) =>
                    lampController.containerList[index],
                mainAxisSpacing: 16,
                crossAxisSpacing: 25,
                itemBuilder: (BuildContext context, int index) =>
                    GestureDetector(
                  onTap: () {
                    Get.to(LampDetailPage(), arguments: [
                      lampController.lampList[index].name,
                      lampController.lampList[index].price,
                      lampController.lampList[index].image,
                      lampController.lampList[index].color,
                      lampController.lampList[index].quntity,
                    ]);
                  },
                  child: Column(
                    children: [
                      new Container(
                        width: Get.width,
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black12,
                              blurRadius: 5,
                              spreadRadius: 2,
                            )
                          ],
                        ),
                        child: Image(
                          image:
                              AssetImage(lampController.lampList[index].image),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            color: Colors.white,
                            width: 80,
                            child: Text(
                              lampController.lampList[index].name,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              style: TextStyle(
                                  fontSize: size.height < 600 ? 12 : 14,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                          Text(
                            "\$${lampController.lampList[index].price.toString()}",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontSize: size.height < 600 ? 14 : 18,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
