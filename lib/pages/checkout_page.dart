import 'package:flutter/material.dart';
import 'package:furniture_app/controller/checkoutcontroller.dart';
import 'package:furniture_app/controller/ordercontroller.dart';
import 'package:get/get.dart';

class CheckoutPage extends StatelessWidget {
  final CheckoutContoller checkoutContoller = Get.put(CheckoutContoller());
  final OrderController orderController = Get.put(OrderController());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 16,
            ),
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.black,
            ),
          ),
        ),
        title: Text(
          "Check Out",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Icon(Icons.close, color: Colors.black),
          )
        ],
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 15,
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              child: Text(
                "Shipping To",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              height: 100,
              width: 450,
              // color: Colors.blueAccent,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: checkoutContoller.shippingList.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        checkoutContoller.isselected.value = index;
                      },
                      child: Container(
                        margin: EdgeInsets.all(16),
                        height: 50,
                        width: 300,
                        decoration: BoxDecoration(
                            color: Color(0xffF7EFE8),
                            // color: Colors.brown.shade100,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Container(
                              height: 45,
                              width: 45,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: Image(
                                image: AssetImage(checkoutContoller
                                    .shippingList[index].icon1),
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      checkoutContoller.shippingList[index].apt,
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(width: 5),
                                    Text(
                                      checkoutContoller
                                          .shippingList[index].address,
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Text(
                                      checkoutContoller
                                          .shippingList[index].city,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.normal),
                                    ),
                                    SizedBox(width: 5),
                                    Text(
                                      checkoutContoller
                                          .shippingList[index].code,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.normal),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Obx(() =>
                                checkoutContoller.isselected.value == index
                                    ? Icon(Icons.check_circle)
                                    : Icon(
                                        Icons.circle_sharp,
                                        color: Colors.transparent,
                                      ))
                          ],
                        ),
                      ),
                    );
                  }),
            ),
            SizedBox(
              height: 15,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Text(
                "Payment Method",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Container(
                height: 300,
                width: 400,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      blurRadius: 5,
                      spreadRadius: 2,
                    )
                  ],
                ),
                child: ListView.builder(
                    itemCount: checkoutContoller.paymentList.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          checkoutContoller.payment.value = index;
                        },
                        child: Container(
                          margin: EdgeInsets.all(8),
                          height: 80,
                          width: 300,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border(
                                  bottom: BorderSide(
                                      width: 1, color: Colors.black12))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              CircleAvatar(
                                radius: 30,
                                backgroundColor: Color(0xffF7EFE8),
                                child: Image(
                                  image: AssetImage(checkoutContoller
                                      .paymentList[index].logo),
                                ),
                              ),

                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    checkoutContoller.paymentList[index].name,
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    checkoutContoller.paymentList[index].card,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ],
                              ),
                              // Obx(() => checkoutContoller.payment.value == index
                              //     ? Image(
                              //         image: AssetImage(checkoutContoller
                              //             .paymentList[index].correct))
                              //     : Container())

                              Obx(() => checkoutContoller.payment.value == index
                                  ? Icon(Icons.check_circle)
                                  : Icon(
                                      Icons.circle_sharp,
                                      color: Colors.transparent,
                                    ))
                            ],
                          ),
                        ),
                      );
                    }),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Total Amount:",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  Obx(
                    () => Text(
                      "\$${orderController.orderList.length == 0 ? 0.0 : (double.parse(orderController.calculateTotal().toStringAsFixed(2)) + orderController.freeShippingAmount.value).toStringAsFixed(2)}",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: SizedBox(
                height: size.height * .06,
                width: size.width * .8,
                child: ElevatedButton(
                    onPressed: () {
                      Get.to(CheckoutPage());
                    },
                    style: ElevatedButton.styleFrom(primary: Color(0xffB86B28)),
                    child: Center(
                        child: Text(
                      "Payment",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ))),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
