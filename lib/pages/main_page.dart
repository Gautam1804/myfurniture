import 'package:flutter/material.dart';
import 'package:furniture_app/controller/bottomcontroller.dart';
import 'package:furniture_app/controller/homecontroller.dart';
import 'package:furniture_app/pages/home_page.dart';
import 'package:furniture_app/pages/menu_page.dart';
import 'package:furniture_app/pages/mycart_page.dart';
import 'package:furniture_app/pages/myprofile_page.dart';

import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';

class MainPage extends StatelessWidget {
  final HomeController homeController = Get.put(HomeController());
  final BottomController bottomController = Get.put(BottomController());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: false,
        title: Obx(() => homeController.isselected.value == 0
            ? Text(
                "Make your best choise",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: size.height < 600 ? 18 : 22,
                    fontWeight: FontWeight.w600),
              )
            : homeController.isselected.value == 1
                ? Text(
                    "Choose your catogory",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: size.height < 600 ? 18 : 22,
                        fontWeight: FontWeight.w600),
                  )
                : homeController.isselected.value == 2
                    ? Text(
                        "My Order",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: size.height < 600 ? 18 : 22,
                            fontWeight: FontWeight.w600),
                      )
                    : Text(
                        "My Account",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: size.height < 600 ? 18 : 22,
                            fontWeight: FontWeight.w600),
                      )),
        actions: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Obx(() => homeController.isselected.value == 0
                ? Image.asset("images/bell.png")
                : homeController.isselected.value == 1
                    ? Image.asset("images/bell.png")
                    : homeController.isselected.value == 2
                        ? Image.asset("images/cancel.png")
                        : Image.asset("images/cancel.png")),
          )
        ],
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: bottomController.pageController,
        children: [HomePage(), MenuPage(), CartPage(), ProfilePage()],
      ),
      bottomNavigationBar: Container(
        height: size.height < 600 ? 50 : 70,
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(color: Colors.black12, blurRadius: 2, spreadRadius: 2)
        ]),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              bottombar("images/home1.png", 0),
              bottombar("images/menu.png", 1),
              bottombar("images/cart.png", 2),
              bottombar("images/user.png", 3),
            ],
          ),
        ),
      ),
    );
  }

  Widget bottombar(String image, int index) => Obx(
        () => InkWell(
            onTap: () {
              bottomController.isselected.value = index;
              bottomController.pageController.animateToPage(index,
                  duration: Duration(milliseconds: 2), curve: Curves.ease);
              homeController.isselected.value = index;
            },
            child: Image.asset(
              image,
              color: bottomController.isselected.value == index
                  ? Color(0xffB86B28)
                  : Color(0xff000000),
            )),
      );
}
