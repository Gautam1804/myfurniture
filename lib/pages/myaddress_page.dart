import 'package:flutter/material.dart';
import 'package:furniture_app/controller/checkoutcontroller.dart';
import 'package:furniture_app/widgets/validation.dart';
import 'package:get/get.dart';

class AddessPage extends StatelessWidget {
  AddessPage({Key? key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();
  final CheckoutContoller checkoutContoller = Get.put(CheckoutContoller());

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    TextEditingController streetAddressController = TextEditingController();
    TextEditingController aptController = TextEditingController();
    TextEditingController cityController = TextEditingController();
    TextEditingController codeController = TextEditingController();

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
        ),
        title: Text(
          "My Address",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: Icon(Icons.close, color: Colors.black),
          )
        ],
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 50,
                ),
                Label(name: "Full Name"),
                Ans(
                  ans: "Full name is required",
                  require: "",
                  email: "",
                  field: "",
                  password: '',
                ),
                SizedBox(height: 30),
                Label(name: "Street Address"),
                Ans(
                  textEditingController: streetAddressController,
                  ans: "Enter your VAlid Address",
                  require: "",
                  email: "",
                  field: '',
                  password: '',
                ),
                SizedBox(height: 30),
                Label(name: "Apt / Other"),
                Ans(
                  textEditingController: aptController,
                  ans: "Enter your Aptmnet number",
                  require: "",
                  email: "",
                  field: '',
                  password: '',
                ),
                SizedBox(height: 30),
                Label(name: "City"),
                Ans(
                  textEditingController: cityController,
                  ans: "Entar your City name",
                  require: "",
                  email: "",
                  field: '',
                  password: '',
                ),
                SizedBox(height: 30),
                Label(name: "Postal code"),
                Ans(
                  textEditingController: codeController,
                  ans: "valid postal code",
                  require: "",
                  email: "",
                  field: '',
                  password: '',
                ),
                SizedBox(height: 50),
                Center(
                  child: SizedBox(
                    height: size.height * .06,
                    width: size.width * .8,
                    child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            Get.back();
                            checkoutContoller.addressListadd(
                                streetAddress: streetAddressController.text,
                                apt: aptController.text,
                                city: cityController.text,
                                code: codeController.text.toString());
                            streetAddressController.clear();
                            aptController.clear();
                            cityController.clear();
                            codeController.clear();
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            primary: Color(0xffB86B28)),
                        child: Center(
                            child: Text(
                          "Save",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ))),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
