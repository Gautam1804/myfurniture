import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:furniture_app/controller/bottomcontroller.dart';
import 'package:furniture_app/controller/homecontroller.dart';
import 'package:furniture_app/controller/imagescontroller.dart';
import 'package:furniture_app/controller/lampcontroller.dart';
import 'package:furniture_app/controller/ordercontroller.dart';
import 'package:furniture_app/pages/image_page.dart';

import 'package:get/get.dart';

class LampDetailPage extends StatelessWidget {
  final ImageController imageController = Get.put(ImageController());
  final LampController lampController = Get.put(LampController());
  final BottomController bottomController = Get.put(BottomController());
  final OrderController orderController = Get.put(OrderController());
  final HomeController homeController = Get.put(HomeController());

  // final PageController pageController = Get.put(PageController());

  @override
  Widget build(BuildContext context) {
    var data = Get.arguments;
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Image.asset("images/back.png")),

        // actions: [Icon(Icons.share, color: Colors.black)],
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                print("object");
                Get.to(ImagePage());
              },
              child: Container(
                child: CarouselSlider(
                  options: CarouselOptions(
                      height: size.height * .4,
                      viewportFraction: 1,
                      autoPlay: true,
                      enlargeCenterPage: true,
                      // aspectRatio: 2.0,
                      onPageChanged: (index, reason) {
                        imageController.current.value = index;
                      }),
                  items: [0, 1, 2, 3].map((i) {
                    return Builder(
                      builder: (BuildContext context) {
                        return Container(
                            width: size.width,
                            // margin: EdgeInsets.symmetric(horizontal: 5.0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                            ),
                            child: Image(
                              image: AssetImage(data[2].toString()),
                              fit: BoxFit.cover,
                            ));
                      },
                    );
                  }).toList(),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 16),
              child: Row(
                children: [
                  indicator(0),
                  SizedBox(
                    width: 5,
                  ),
                  indicator(1),
                  SizedBox(
                    width: 5,
                  ),
                  indicator(2),
                  SizedBox(
                    width: 5,
                  ),
                  indicator(3),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16),
              child: Text(
                data[0].toString(),
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                "\$ ${data[1].toString()}",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    color: Color(0xff717171)),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text(
                        "Colors",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        width: size.width * .4,
                      ),
                      Text(
                        "Size",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          colorsbar(Color(0xffA0865A), 0),
                          Padding(padding: EdgeInsets.only(left: 2)),
                          colorsbar(Color(0xff734646), 1),
                          Padding(padding: EdgeInsets.only(left: 2)),
                          colorsbar(Color(0xff25B174), 2),
                          Padding(padding: EdgeInsets.only(left: 2)),
                          colorsbar(Color(0xff687A2B), 3),
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                            "H : 260cm  W : 140cm",
                            style: TextStyle(
                              fontSize: size.height < 600 ? 12 : 15,
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 15),
              child: Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry.typesetting industry",
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w300),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Center(
              child: GestureDetector(
                  onTap: () {},
                  child: Container(
                    height: 50,
                    width: 279,
                    color: Color(0xffB86B28),
                    child: Center(
                        child: Text(
                      "Buy Now",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    )),
                  )),
            ),
            SizedBox(height: 15),
            Center(
              child: GestureDetector(
                  onTap: () {
                    // print("cart");
                    // Navigator.popUntil(context, (route) => false);
                    orderController.addToCart(
                        name: data[0].toString(),
                        color: data[3].toString(),
                        image: data[2].toString(),
                        price: data[1]);

                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.pop(context);

                    bottomController.pageController.animateToPage(2,
                        duration: Duration(microseconds: 2),
                        curve: Curves.ease);

                    bottomController.isselected.value = 2;

                    homeController.isselected.value = 2;
                  },
                  child: Container(
                    height: 50,
                    width: 279,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          top: BorderSide(width: 2.0, color: Color(0xFFB86B28)),
                          left:
                              BorderSide(width: 2.0, color: Color(0xFFB86B28)),
                          right:
                              BorderSide(width: 2.0, color: Color(0xFFB86B28)),
                          bottom:
                              BorderSide(width: 2.0, color: Color(0xFFB86B28)),
                        )),
                    child: Center(
                        child: Text(
                      "Add to cart",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    )),
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Widget colorsbar(Color color, int index) {
    return Obx(
      () => GestureDetector(
        onTap: () {
          imageController.isselected.value = index;
        },
        child: Container(
            height: 35,
            width: 35,
            child: Center(
                child: CircleAvatar(
                    radius: imageController.isselected.value == index ? 14 : 11,
                    backgroundColor: color)),
            decoration: BoxDecoration(
                color: Colors.white,
                shape: BoxShape.circle,
                border: Border.all(
                    color: imageController.isselected.value == index
                        ? Color(0xffC8C8C8)
                        : Colors.white,
                    width: 1))),
      ),
    );
  }

  Widget indicator(int index) {
    return Obx(
      () => Container(
        height: 10,
        width: 40,
        decoration: BoxDecoration(
            color: imageController.current.value == index
                ? Color(0xffB86B28)
                : Color(0xffF0DFD1),
            borderRadius: BorderRadius.all(Radius.circular(50))),
      ),
    );
  }
}
