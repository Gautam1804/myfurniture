import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:furniture_app/pages/login_page.dart';
import 'package:furniture_app/pages/main_page.dart';
import 'package:furniture_app/widgets/validation.dart';
import 'package:get/get.dart';

class RegisterPage extends StatelessWidget {
  RegisterPage({Key? key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          GestureDetector(
              onTap: () {
                Get.to(LoginPage());
              },
              child: Image.asset("images/cancel.png"))
        ],
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 20, bottom: 80),
                  child: Text(
                    "Personal Details",
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                Label(name: "Your Email"),
                Ans(
                  ans: "Email is reqired",
                  require: "",
                  email: "",
                  field: "email",
                  password: '',
                ),
                SizedBox(height: 20),
                Label(name: "Password"),
                Ans(
                  ans: "Password is required",
                  require: "",
                  email: "",
                  field: '',
                  password: "password",
                ),
                SizedBox(height: 20),
                Label(name: "Phone"),
                Ans(
                  ans: "Mobile number is required",
                  require: "",
                  email: "",
                  field: "phone",
                  password: '',
                ),
                SizedBox(height: 20),
                Label(name: "Address"),
                Ans(
                  ans: "Enter valid Address",
                  require: "",
                  email: "",
                  field: '',
                  password: '',
                ),
                SizedBox(height: 20),
                Padding(padding: EdgeInsets.only(top: size.height * .1)),
                Center(
                  child: SizedBox(
                    height: size.height * .06,
                    width: size.width * .8,
                    child: ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          Get.to(MainPage());
                        }
                      },
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)),
                          primary: Color(0xffB86B28)),
                      child: Text(
                        "Register",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Color(0xffFFFFFF)),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: size.height * .12),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RichText(
                        text: TextSpan(
                            text: "Already have account ?",
                            style: TextStyle(color: Colors.black, fontSize: 14),
                            children: [
                          TextSpan(
                            text: " Login",
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                print("register");
                                Get.to(LoginPage());
                              },
                          )
                        ])),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      // bottomNavigationBar: Padding(
      //   padding: EdgeInsets.only(bottom: 30),
      //   child: Row(
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     children: [
      //       RichText(
      //           text: TextSpan(
      //               text: "Already have account ?",
      //               style: TextStyle(color: Colors.black, fontSize: 14),
      //               children: [
      //             TextSpan(
      //               text: " Login",
      //               recognizer: TapGestureRecognizer()
      //                 ..onTap = () {
      //                   print("register");
      //                   Get.to(LoginPage());
      //                 },
      //             )
      //           ])),
      //     ],
      //   ),
      // ),
    );
  }
}
