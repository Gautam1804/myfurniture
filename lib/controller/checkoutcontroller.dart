import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';

class CheckoutContoller extends GetxController {
  var isselected = 0.obs;

  var payment = 0.obs;

  RxList<Shipping> shippingList = RxList([
    Shipping(
        icon1: "images/home.png",
        address: "Harvard Drive",
        city: "Joliet",
        apt: "315",
        code: "IL 60435"),
    // Shipping(
    //     icon1: "images/home.png",
    //     address: "Harvard Drive",
    //     city: "Joliet",
    //     apt: "315",
    //     code: "IL 60435"),
  ]);

  addressListadd(
      {String? streetAddress, String? apt, String? city, String? code}) {
    shippingList.add(Shipping(
        address: streetAddress!,
        apt: apt!,
        city: city!,
        code: code!,
        icon1: "images/home.png"));
  }

  RxList<Payment> paymentList = RxList([
    Payment(
        logo: "images/visa.png",
        name: "****  ****  **** 5651",
        card: "VISA",
        correct: "images/correct.png"),
    Payment(
        logo: "images/paypal.png",
        name: "****  ****  **** 7568",
        card: "Paypal",
        correct: "images/correct.png"),
    Payment(
        logo: "images/mastercard.png",
        name: "****  ****  **** 4565",
        card: "Master Card",
        correct: "images/correct.png"),
  ]);
}

class Shipping {
  String icon1;

  String address;
  String city;
  String apt;
  String code;

  Shipping(
      {this.icon1 = '',
      this.address = '',
      this.city = '',
      this.apt = '',
      this.code = ''});
}

class Payment {
  String logo;
  String name;
  String card;
  String correct;

  Payment({this.logo = '', this.name = '', this.card = '', this.correct = ''});
}
