import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';

class HomeController extends GetxController {
  var isselected = 0.obs;
  RxList<Home> homeList = RxList([
    Home(name: "Eames Chair", image: "images/chair1.png", price: "\$124"),
    Home(name: "Nightstand Chair", image: "images/chair2.png", price: "\$532"),
    Home(name: "Simpal Chair", image: "images/chair1.png", price: "\$99"),
  ]);

  RxList<Special> specialList = RxList([
    Special(name: "Ssfurm Sofa", image: "images/sofa.png", price: "\$124"),
    Special(name: "Eames Chair", image: "images/chair4.png", price: "\$532"),
    Special(name: "Simpal Chair", image: "images/sofa.png", price: "\$99"),
  ]);
}

class Home {
  String name;
  String image;
  String price;

  Home({this.name = '', this.image = '', this.price = ""});
}

class Special {
  String name;
  String image;
  String price;

  Special({this.name = '', this.image = '', this.price = ""});
}
