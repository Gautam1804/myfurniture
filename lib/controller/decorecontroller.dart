import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';

class DecoreController extends GetxController {
  RxList<Decore> decoreList = RxList([
    Decore(image: "images/d1.png", name: "Floor Lamps"),
    Decore(image: "images/d2.png", name: "Hanging Lamps"),
    Decore(image: "images/d3.png", name: "Sconces Lamps"),
    Decore(image: "images/d4.png", name: "Table Lamps"),
    Decore(image: "images/d5.png", name: "Chandelier"),
    Decore(image: "images/d6.png", name: "Garlands"),
    Decore(image: "images/d1.png", name: "Floor Lamps"),
    Decore(image: "images/d2.png", name: "Hanging Lamps"),
    Decore(image: "images/d3.png", name: "Sconces Lamps"),
    Decore(image: "images/d4.png", name: "Table Lamps"),
    Decore(image: "images/d5.png", name: "Chandelier"),
    Decore(image: "images/d6.png", name: "Garlands"),
  ]);
}

class Decore {
  String image;
  String name;

  Decore({this.image = '', this.name = ''});
}
