import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';

class OrderController extends GetxController {
  @override
  void onInit() {
    super.onInit();
    calculateTotal();
  }

  RxBool freeShipping = true.obs;
  RxInt freeShippingAmount = 40.obs;
  RxList<Order> orderList = RxList([
    // Order(
    //     name: "Braided belly lamp ",
    //     image: "images/m1.png",
    //     color: "Brown",
    //     quntity: 1.obs,
    //     price: 567),
    // Order(
    //     name: "Mariyo Golden lamp ",
    //     image: "images/m2.png",
    //     color: "Black",
    //     quntity: 1.obs,
    //     price: 56),
  ]);

  addToCart({String? name, String? image, String? color, double? price}) {
    orderList.add(Order(
        color: color!,
        name: name!,
        image: image!,
        price: price!,
        quntity: 1.obs));
  }

  double calculateTotal() {
    RxDouble? total = 0.0.obs;
    for (int i = 0; i < orderList.length; i++) {
      total.value += orderList[i].price * orderList[i].quntity!.value;
    }
    return total.value;
  }
}

class Order extends GetxController {
  String name;
  String image;
  String color;
  double price;
  RxInt? quntity;

  Order(
      {this.name = '',
      this.image = '',
      this.color = '',
      this.price = 0,
      this.quntity});
}
