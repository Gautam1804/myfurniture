import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class MenuController extends GetxController {
  RxList<Menu> menuList = RxList([
    Menu(title: "Furnishing", image: "images/c1.png"),
    Menu(title: "Decor", image: "images/c2.png"),
    Menu(title: "Bedroom", image: "images/c3.png"),
    Menu(title: "Dining", image: "images/c4.png"),
    Menu(title: "Kids", image: "images/c5.png"),
    Menu(title: "Bathroom", image: "images/c6.png"),
    Menu(title: "Kitchen", image: "images/c7.png"),
    Menu(title: "Wooden", image: "images/c8.png"),
    Menu(title: "Other", image: "images/c9.png"),
    Menu(title: "Other", image: "images/c9.png"),
    Menu(title: "Other", image: "images/c9.png"),
    Menu(title: "Other", image: "images/c9.png"),
    Menu(title: "Other", image: "images/c9.png"),
    Menu(title: "Other", image: "images/c9.png"),
    Menu(title: "Other", image: "images/c9.png"),
  ]);
}

class Menu {
  String title;
  String image;

  Menu({this.title = '', this.image = ''});
}
