import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';

class LampController extends GetxController {
  RxList<Lamp> lampList = RxList([
    Lamp(
      name: "Lantern Cultural",
      image: "images/h1.png",
      price: 65,
      color: "red",
      quntity: 1.obs,
    ),
    Lamp(
      name: "Lantern Cultural",
      image: "images/h2.png",
      price: 74.4,
      color: "red",
      quntity: 1.obs,
    ),
    Lamp(
      name: "Braided belly lamp",
      image: "images/h3.png",
      price: 32.4,
      color: "red",
      quntity: 1.obs,
    ),
    Lamp(
      name: "Willow Pendent",
      image: "images/h4.png",
      price: 75.6,
      color: "red",
      quntity: 1.obs,
    ),
    Lamp(
      name: "Braided belly lamp",
      image: "images/h5.png",
      price: 34.6,
      color: "red",
      quntity: 1.obs,
    ),
    Lamp(
      name: "Willow Pendent",
      image: "images/h6.png",
      price: 74.8,
      color: "red",
      quntity: 1.obs,
    ),
    Lamp(
      name: "Lantern Cultural",
      image: "images/h1.png",
      price: 65,
      color: "red",
      quntity: 1.obs,
    ),
    Lamp(
      name: "Lantern Cultural",
      image: "images/h2.png",
      price: 74.8,
      color: "red",
      quntity: 1.obs,
    ),
    Lamp(
      name: "Braided belly lamp",
      image: "images/h3.png",
      price: 76.8,
      color: "red",
      quntity: 1.obs,
    ),
    Lamp(
      name: "Willow Pendent",
      image: "images/h4.png",
      price: 75.6,
      color: "red",
      quntity: 1.obs,
    ),
    Lamp(
      name: "Braided belly lamp",
      image: "images/h5.png",
      price: 34.6,
      color: "red",
      quntity: 1.obs,
    ),
    Lamp(
      name: "Willow Pendent",
      image: "images/h6.png",
      price: 74.8,
      color: "red",
      quntity: 1.obs,
    ),
  ]);

  List containerList = [
    StaggeredTile.count(
      2,
      2.5,
    ),
    StaggeredTile.count(
      2,
      2.8,
    ),
    StaggeredTile.count(
      2,
      3.4,
    ),
    StaggeredTile.count(
      2,
      2.8,
    ),
    StaggeredTile.count(
      2,
      3,
    ),
    StaggeredTile.count(
      2,
      2,
    ),
    StaggeredTile.count(
      2,
      2.5,
    ),
    StaggeredTile.count(
      2,
      2.8,
    ),
    StaggeredTile.count(
      2,
      3.4,
    ),
    StaggeredTile.count(
      2,
      2.8,
    ),
    StaggeredTile.count(
      2,
      2.8,
    ),
    StaggeredTile.count(
      2,
      3,
    )
  ];
}

class Lamp {
  String name;
  String image;
  double price;
  String color;
  RxInt? quntity;

  Lamp(
      {this.name = '',
      this.image = '',
      required this.price,
      this.color = '',
      this.quntity});
}
