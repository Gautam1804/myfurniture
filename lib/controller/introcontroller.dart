import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';

class IntroController extends GetxController {
  var current = 0.obs;

  // RxString name = "".obs;
  // RxString subtitle = "".obs;
  // RxString image = "".obs;

  RxList<Intro> introList = RxList([
    Intro(
      name: "Discover",
      subtitle:
          "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
      image: "images/discover.jpeg",
    ),
    Intro(
      name: "New Arrivals",
      subtitle:
          "Lorem Ipsum is simply dummy text of theprinting and typesetting industry.",
      image: "images/arrive.jpeg",
    ),
    Intro(
      name: "Ready Set",
      subtitle:
          "Lorem Ipsum is simply dummy tex and typesetting industry.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a mor",
      image: "images/ready.jpeg",
    )
  ]);
}

class Intro {
  String name;
  String subtitle;
  String image;

  Intro({this.name = '', this.subtitle = '', this.image = ''});
}
