import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/state_manager.dart';

class ImageController extends GetxController {
  var isselected = 0.obs;
  var current = 0.obs;
  RxList<Pics> imageList = RxList([
    Pics(image: "images/belly1.png"),
    Pics(image: "images/belly2.jpeg"),
    Pics(image: "images/belly3.jpeg"),
    Pics(image: "images/belly4.jpeg"),
  ]);
}

class Pics {
  String image;

  Pics({this.image = ''});
}
