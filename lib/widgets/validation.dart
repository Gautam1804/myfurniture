import 'package:flutter/material.dart';

class Label extends StatelessWidget {
  final String name;

  const Label({Key? key, required this.name}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      name,
      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
    );
  }
}

class Ans extends StatelessWidget {
  final String ans;
  final String require;
  final String email;
  final String? field;
  final String password;
  final TextEditingController? textEditingController;

  const Ans(
      {Key? key,
      required this.ans,
      required this.require,
      required this.email,
      required this.field,
      this.textEditingController,
      required this.password})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: textEditingController,
      validator: (value) {
        if (value!.isEmpty) {
          return (ans);
        } else if (field == "email") {
          if (!RegExp(
                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
              .hasMatch(value.toString())) {
            return "Email is not valid";
          }
        } else if (field == "password") {
          if (value.length < 6) {
            return "Password is not valid";
          }
        } else if (field == "phone") {
          if (value.length != 10) {
            return "enter the valid number";
          }
        }
      },
      cursorColor: Color(0xffB86B28),
      decoration: InputDecoration(
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xffB86B28),
          ),
        ),
      ),
    );
  }
}
