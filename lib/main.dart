import 'package:flutter/material.dart';

import 'package:furniture_app/pages/intro_page.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: 'Furniture App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: IntroPage());
  }
}
